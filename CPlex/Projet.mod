/*********************************************
 * OPL 12.10.0.0 Model
 * Author: valen
 * Creation Date: 31 janv. 2021 at 14:11:48
 *********************************************/
using CP;

int N = ...;

range rangeCarre = 1..N;

dvar int+ carreMagique[rangeCarre][rangeCarre] in 1..N*N;

maximize carreMagique[1][5] + carreMagique[2][3] + carreMagique[3][6] + carreMagique[4][4] + carreMagique[5][1] + carreMagique[6][2];


subject to {
  BETWEEN_1_9 : forall(i in rangeCarre) 
  					forall(j in rangeCarre)
  					  carreMagique[i][j] >= 1 && carreMagique[i][j] <= (N * N);
  ALL_DIFFERENTS : allDifferent (carreMagique);
  SAME_SUM_LINES : forall(i in rangeCarre)
  					sum(j in rangeCarre)
  					  carreMagique[i][j] == sum(j in rangeCarre) carreMagique[1][j];
  SAME_SUM_COLUMNS : forall(i in rangeCarre)
  						sum(j in rangeCarre)
  					  		carreMagique[j][i] == sum(j in rangeCarre) carreMagique[1][j];
}