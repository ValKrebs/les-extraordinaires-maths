﻿using System;

namespace LesExtraordinaires
{
    public static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Knight's tour solver");
            bool shouldQuit = false;
            string userInput;

            while (!shouldQuit)
            {
                try
                {
                    Console.WriteLine("Board size ? ");
                    int boardSize = int.Parse(Console.ReadLine());
                    Console.WriteLine("Starting cell x : ");
                    int x = int.Parse(Console.ReadLine());
                    Console.WriteLine("Starting cell y : ");
                    int y = int.Parse(Console.ReadLine());
                    Board board = new Board(6);
                    Console.WriteLine("Solving...");
                    board.Solve(x, y);
                    Console.WriteLine("Finished!");
                }
                catch
                {
                    Console.WriteLine("Input error, please enter valid values!");
                }
            }
            
        }
    }
}
