﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Reflection.Metadata.Ecma335;
using System.Threading;
using LesExtraordinaires.Utils;

namespace LesExtraordinaires
{
    public class Board
    {
        private readonly Coordinates[] _knightPossibleMovements =
        {
            new(2, 1),
            new(1, 2),
            new(-1, 2),
            new(-2, 1),
            new(-2, -1),
            new(-1, -2),
            new(1, -2),
            new(2, -1)
        };

        public Board(int size)
        {
            Size = size;
            Cells = new Cell[size, size];
            Paths = new List<Path>();

            InitCellsArray();
            ComputeLinkedCells();
        }

        internal Cell[,] Cells { get; }
        internal int[] VisitedCells { get; private set; }
        internal int Size { get; }
        internal List<Path> Paths { get; set; }
        internal Path Path { get; set; }

        private int _squaredSize => Size * Size;

        private void InitCellsArray()
        {
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    Cells[i, j] = new Cell(new Coordinates(i, j));
                }
            }
        }

        public Cell GetCell(Coordinates coordinates)
        {
            return IsInBounds(coordinates) ? Cells[coordinates.X, coordinates.Y] : null;
        }

        private bool IsInBounds(Coordinates coordinates)
        {
            return coordinates.X >= 0 && coordinates.X < Size && coordinates.Y >= 0 && coordinates.Y < Size;
        }

        public void ComputeLinkedCells()
        {
            foreach (Cell cell in Cells)
            {
                foreach (Coordinates knightPossibleMovement in _knightPossibleMovements)
                {
                    Coordinates landingCoordinates = cell.Coordinates + knightPossibleMovement;
                    Cell landingCell = GetCell(landingCoordinates);

                    if (landingCell != null) cell.AddReachableCell(landingCell);
                }
            }
        }

        public void SolveFromRandomPoint()
        {
            Random random = new Random(Guid.NewGuid().GetHashCode());
            
            int x = random.Next(Size);
            int y = random.Next(Size);

            Console.WriteLine("Randomly selected starting cell.");
            Solve(x, y);
        }
        
        public void Solve(int x, int y)
        {
            Cell cellStart = GetCell(new Coordinates(x, y));
            Console.WriteLine($"Starting cell : {x};{y}");
            Path = new Path(this, null, cellStart);
            bool res = ImprovedSolve(Path);
        }

        public bool Solve(Path path)
        {
            if (path.HasVisitedEveryCell() && AreLinked(path._newCell, path._startCell))
            {
                Console.WriteLine(path);
                return true;
            }
            else
            {
                foreach (Cell cellReachableCell in path._newCell.ReachableCells)
                {
                    if (!IsVisited(path, cellReachableCell) && Solve(new Path(this, path, cellReachableCell)))
                        return true;
                }
            }

            return false;
        }

        public bool ImprovedSolve(Path path)
        {
            if (path.HasVisitedEveryCell() && AreLinked(path._newCell, path._startCell))
            {
                Console.WriteLine(path);
                return true;
            }
            else
            {
                IList<Cell> orderedCells = path._newCell.ReachableCells.OrderBy(c => GetDegree(path, c)).ToList();

                foreach (Cell cellReachableCell in orderedCells)
                {
                    if (!IsVisited(path, cellReachableCell) && ImprovedSolve(new Path(this, path, cellReachableCell)))
                                return true;
                }
            }
            return false;
        }
        
        
        public static bool AreLinked(Cell cellFrom, Cell cellTo)
        {
            return cellFrom.ReachableCells.Contains(cellTo);
        }
        
        public static int GetDegree(Path path, Cell cell)
        {
            return cell.ReachableCells.Count(reachableCell => !IsVisited(path, reachableCell));
        }

        private static bool IsVisited(Path path, Cell cell)
        {
            return path._visited[cell.Coordinates.X, cell.Coordinates.Y];
        }
    }
}