﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using LesExtraordinaires.Utils;

namespace LesExtraordinaires
{
    public class Cell
    {
        private static readonly string abcdef = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        public IList<Cell> ReachableCells { get; set; }

        public Cell(Coordinates coordinates)
        {
            Coordinates = coordinates;
            ReachableCells = new List<Cell>();
        }

        public Coordinates Coordinates { get; }

        public bool AddReachableCell([NotNull] Cell cell)
        {
            if (cell == null || ReachableCells.Contains(cell))
            {
                return false;
            }

            ReachableCells.Add(cell);
            return true;
        }

        public override string ToString()
        {
            return $"[{abcdef[Coordinates.X]}{Coordinates.Y}]";
        }
    }
}