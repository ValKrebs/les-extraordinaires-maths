﻿using System.Linq;

namespace LesExtraordinaires
{
    public class Path
    {
        internal Board _board;
        internal int _length;
        internal Cell _newCell;
        internal Path _pathPred;
        internal Cell _startCell;
        internal bool[,] _visited;

        public Path(Board board, Path pathPred, Cell cell)
        {
            _board = board;
            _pathPred = pathPred;
            _newCell = cell;

            if (pathPred == null)
                _length = 1;
            else
                _length = pathPred._length + 1;

            int size = board.Size;
            _visited = new bool[size, size];
            
            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                    if (pathPred == null)
                    {
                        _visited[i, j] = false;
                        _startCell = cell;
                    }
                    else
                    {
                        _visited[i, j] = _pathPred._visited[i, j];
                        _startCell = _pathPred._startCell;
                    }

            int x = cell.Coordinates.X;
            int y = cell.Coordinates.Y;
            _visited[x, y] = true;
        }

        internal bool HasVisitedEveryCell()
        {
            return _visited.Cast<bool>().All(b => b);
        }

        public override string ToString()
        {
            string strPath = _pathPred != null ? $"{_pathPred} - " : "Path = ";
            strPath += _newCell.ToString();
            return strPath;
        }
    }
}