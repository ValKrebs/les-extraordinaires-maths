using LesExtraordinaires.Utils;
using Xunit;

namespace LesExtraordinaires.Test
{
    public class UnitTest1
    {
        [Theory]
        [InlineData(0, 0, 2, 1)]
        [InlineData(0, 0, 1, 2)]
        [InlineData(3, 2, 2, 0)]
        [InlineData(3, 2, 4, 0)]
        [InlineData(3, 2, 1, 1)]
        [InlineData(3, 2, 1, 3)]
        [InlineData(3, 2, 2, 4)]
        [InlineData(3, 2, 4, 4)]
        [InlineData(3, 2, 5, 1)]
        [InlineData(3, 2, 5, 3)]
        public void TestAreLinkedCells(int Cell1X, int Cell1Y, int Cell2X, int Cell2Y)
        {
            Coordinates Cell1Coordinates = new(Cell1X, Cell1Y);
            Coordinates Cell2Coordinates = new(Cell2X, Cell2Y);

            Board board = new(6);
            board.ComputeLinkedCells();
            bool areLinked = Board.AreLinked(board.GetCell(Cell1Coordinates), board.GetCell(Cell2Coordinates));
            Assert.True(areLinked);
        }

        [Theory]
        [InlineData(0, 0, 1, 1)]
        [InlineData(0, 0, 1, 3)]
        [InlineData(3, 2, 3, 3)]
        [InlineData(3, 2, 4, 2)]
        [InlineData(3, 2, -1, 0)]
        [InlineData(3, 2, -1, -1)]
        public void TestAreNotLinkedCells(int Cell1X, int Cell1Y, int Cell2X, int Cell2Y)
        {
            Coordinates Cell1Coordinates = new(Cell1X, Cell1Y);
            Coordinates Cell2Coordinates = new(Cell2X, Cell2Y);

            Board board = new(6);
            board.ComputeLinkedCells();
            bool areLinked = Board.AreLinked(board.GetCell(Cell1Coordinates), board.GetCell(Cell2Coordinates));
            Assert.False(areLinked);
        }
    }
}