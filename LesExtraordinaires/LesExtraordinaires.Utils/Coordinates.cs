﻿namespace LesExtraordinaires.Utils
{
    public readonly struct Coordinates
    {
        public Coordinates(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; }
        public int Y { get; }

        public static Coordinates operator +(Coordinates coordinates1, Coordinates coordinates2)
        {
            return new(coordinates1.X + coordinates2.X, coordinates1.Y + coordinates2.Y);
        }
    }
}